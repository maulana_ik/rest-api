<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\support\Str;
use App\Traits\UsesUuid;
use Auth;
use App\Otp;
use App\Role;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use UsesUuid;

    // protected function get_admin_id()
    // {

    //     // $this->belongsTo('App\Role');

    //     $role = \App\Role::where('name','admin')->first();
    //     return $role->id;
    // }

    // public static function boot()
    // {
        

    //     static::creating(function ($model) 
    //     {
    //         $model->role_id = $model->get_admin_id;
    //     });
    // }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isAdmin() {
        //  $role = Auth::user()->role();

        if(Auth::user()->role->name == 'admin'){
            return true;
        }
        return false;
    }

    public function isVerified () {
        //  $role = Auth::user()->role();

        if($this->email_verified_at !== null){
            return true;
            
        }
        return false;
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otp()
    {
        return $this->hasOne('App\Otp');
    }

}
