<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Otp extends Model
{
    use UsesUuid;
    protected $guarded = [];

    protected $table = "otp_codes";


    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
